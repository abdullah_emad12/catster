# Catster Chatting App 

A simple client-server chatting App built with java TCP sockets that uses a simple protocol.

## Dependencies

In order to run and compile the program java >= 8 needs to be installed on your machine. It is also highly recommeded to have eclipse installed.

## How to Run

**First you will need to run the server**

1. open the terminal and `cd path/to/project/Executables`.
2. `java -jar catster_server.jar PORT_NUMBER` 
	where PORT_NUMBER can be any unused port number
 
**Then you will need to start up the chatting App**

1. open the terminal and `cd path/to/project/Executables`.
2. `java -jar catster.jar`
3. you will be prompted for a user name; enter any username you would like to have.
4. In the IP address field you should enter the ip of your localhost concatenated with the port number you chose when running the server.
	e.g: `127.0.0.1:1200` where 1200 is the port number

Note: on windows it might be enough to double click the caster.jar file instead of using the command line but note that you will still have to provide the port number as an argument to the server.


## Project: 


This project has two main seperate parts: 
1. A server that routes messages from and to connected clients.
2. A chatting app that connects to the server and sends messages received from the GUI to the server.


## Docs: 
1. [server](server/doc/index.html)
2. [App](app/doc/index.html)


## Disclaimer: 

<div>Logo made with <a href="https://www.designevo.com/" title="Free Online Logo Maker">DesignEvo</a></div> 

