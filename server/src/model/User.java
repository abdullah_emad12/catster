package model;

import java.net.Socket;

/**
 * 
 * @author Abdullah Emad 
 * 
 * This class represents a user that has established a TCP connection with the server
 *
 */
public class User {

	private String name;
	Socket socket;
	public User(String name,Socket socket)
	{
		this.name = name;
		this.socket = socket;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Socket getSocket() {
		return socket;
	}
	
}
