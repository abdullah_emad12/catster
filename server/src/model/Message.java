package model;

public class Message {
	
	private String sender;
	private String receiver; 
	private String content;
	private int ttl;
	
	public Message(String sender, String receiver, String content, int ttl)
	{
		this.sender = sender;
		this.receiver = receiver;
		this.content = content;
		this.ttl = ttl;
	}
	
	public String getMessage()
	{
		return "SEND$" + sender + "$" + receiver + "$" + content + "$" + ttl + "\n";
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getTtl() {
		return ttl;
	}

	public void setTtl(int ttl) {
		this.ttl = ttl;
	}
}
