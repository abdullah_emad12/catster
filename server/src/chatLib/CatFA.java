package chatLib;


import java.io.IOException;

import model.Message;

/**
 * 
 * @author Abdullah Emad
 * This class represents a finite state machine that acts on input from the user and calls
 * the appropriate method in the RequestHandler received the input
 * 
 *
 */
public class CatFA {
	private StateFA currentState;
	private RequestHandler handler;
	
	public CatFA(RequestHandler handler)
	{
		currentState = StateFA.CONNECTED;
		this.handler = handler;
	}
	
	public void onInput(String input) throws IOException
	{
		switch(currentState)
		{
			case CONNECTED:
				parseConnected(input);
				break;
			case JOINED:
				parseJoin(input);
				break;
			default:
				break;
		
		}
	}
	public StateFA getCurrentState()
	{
		return this.currentState;
	}
	/**
	 * String -> void
	 * parses the expected input when in the connected state and takes necessary actions
	 * 
	 * String input: the input that was sent by the client
	 * @throws IOException 
	 */
	private void parseConnected(String input) throws IOException
	{
		// requested a list of online people
		if(input.equals("GET"))
		{
			handler.GetMemberListResponse();
			return;
		}
		// dont waste my time
		if(input.length() < 5)
		{
			handler.sendError();
			return;
		}
		
		// the first 4 letters must be "JOIN" indicating a join request
		if(!input.substring(0, 4).equals("JOIN"))
		{
			handler.sendError();
			return;
		}
		
		
		// parses the user name
		String username = "";
		for(int i = 4; i < input.length(); i++)
		{
			if(input.charAt(i) == '$' && i + 1 < input.length())
			{
				username = input.substring(i + 1, input.length());
				break;
			}
		}
		// must have some kind of username
		if(username == "")
		{
			handler.sendError();
			return;
		}
		
		if(handler.getServer().getUser(username) != null)
		{
			handler.sendError();
			return;
		}
		currentState = StateFA.JOINED;
		handler.JoinResponse(username);
	}

	

	/**
	 * String -> void
	 * parses the expected input when in the JOINED state and takes necessary actions
	 * 
	 * String input: the input that was sent by the client
	 * @throws IOException 
	 */
	private void parseJoin(String input) throws IOException
	{
		// terminates the connection
		if(input.equals("BYE"))
		{
			currentState = StateFA.DISCONNECTED;
			return;
		}
		
		// requested a list of online people
		if(input.equals("GET"))
		{
			handler.GetMemberListResponse();
			return;
		}
		
		
		// dont waste my time
		if(input.length() < 12)
		{
			handler.sendError();
			return;
		}
		
		// the first 4 letters must be "JOIN" indicating a join request
		if(!input.substring(0, 4).equals("SEND"))
		{
			handler.sendError();
			return;
		}
		
		String[] args = input.split("\\$");
		
		/*badly formatted message*/
		if(args.length != 5)
		{
			handler.sendError();
			return;
		}
		boolean numeric = args[4].matches("-?\\d+(\\.\\d+)?");
		
		// checkes that the ttl is numeric
		if(!numeric)
		{
			handler.sendError();
			return;
		}
		int ttl = Integer.parseInt(args[4]);
		ttl--;
		
		// drops message
		if(ttl == 0)
		{
			handler.sendError();
			return;
		}
		Message message = new Message(args[1], args[2], args[3],ttl);
		handler.route(message);
		
	}

}
