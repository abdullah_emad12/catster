package chatLib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

import model.User;

/**
 * 
 * @author Abdullah
 * This Class is responsible for getting all the online users that are connected to
 * the other server
 *
 */
public class UsersRefresher extends Thread {

	ArrayList<User> users;
	int serverport;
	public static final int sleepTime = 3000; 
	public UsersRefresher(ArrayList<User> users, int serverPort) 	
	{
		this.users = users;
		this.serverport = serverPort;
	}
	public void run()
	{
		while(true)
		{
			
			try
			{
				sleep(sleepTime);
				Socket socket = new Socket("127.0.0.1", serverport);
				socket.setKeepAlive(false);

				DataOutputStream  outToClient = new DataOutputStream(socket.getOutputStream());
				outToClient.writeBytes("GET\n");
          	  
				BufferedReader inFromServer = 
    	                new BufferedReader(new
    	                InputStreamReader(socket.getInputStream())); 
				String response = inFromServer.readLine(); 
				parseList(response);
				socket.close();
			}
			catch(Exception e)
			{
			}
		}
	}
	
	public void parseList(String list)
	{
		if(!list.substring(0,4).equals("LIST") || list.length() <= 5)
		{
			return;
		}
		
		String[] sepList = list.split("\\$");
		for(int i = 1; i < sepList.length; i++)
		{
			boolean contains =  false;
			for(User user : users)
			{
				if(user.getName().equals(sepList[i]))
				{
					contains = true;
				}
			}
			if(!contains)
			{
				users.add(new User(sepList[i], null));
			}
		}
		
		// filters out disconnected users
		for(User user : users)
		{
			if(user.getSocket() == null)
			{
				boolean contains =  false;

				for(int i = 1; i < sepList.length; i++)
				{
					if(user.getName().equals(sepList[i]))
					{
						contains = true;
					}
				}
				if(!contains)
				{
					users.remove(user);
				}
				
			}
		}
	}
}
