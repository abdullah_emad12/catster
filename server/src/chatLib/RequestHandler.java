package chatLib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import main.Server;
import model.Message;
import model.User;

/**
 * 
 * @author Abdullah Emad
 * 
 * This class is responsible for handling all the interactions with a socket independent
 * from the server
 */
public class RequestHandler extends Thread {
	
	private Socket socket;
	private Server server;
	private User user;
	private CatFA machine;
	
	
	public RequestHandler(Socket socket, Server server)
	{
		this.socket = socket;
		this.server = server; 
		this.machine = new CatFA(this);
		user = null;
	}
	
	/**
	 * void -> void
	 * This function is responsible for listening to the user socket 
	 * Note: This function is called by a new thread
	 * 
	 * 
	 */
	public void run()
	{
		while(this.machine.getCurrentState() != StateFA.DISCONNECTED)
		{
			BufferedReader inFromClient = null;
			try {
				inFromClient = new BufferedReader(new
				        InputStreamReader(socket.getInputStream()));
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}
			
			
		 	try {
		 		
		 		// reads input from the client
				
	             String input = inFromClient.readLine();
	             if(input == null)
	             {
	            	 break;
	             }
		          this.machine.onInput(input);
	             if(this.machine.getCurrentState() == StateFA.DISCONNECTED)
	             {
	            	 DataOutputStream  outToSender = new DataOutputStream(this.socket.getOutputStream()); 
	        		 outToSender.writeBytes("BYE\n");
	             }
	             
			} catch (IOException e) {
				e.printStackTrace();
			} 
		 	
		}
		
		/*Closes the connection and removes the user from the list*/
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		server.removeUser(user);
		
	}
	
	/**
	 * String -> void
	 *  This gets called when the client sends JOIN request to the server
	 *  
	 *   String username: the chosen username
	 * @throws IOException 
	 */
	void JoinResponse(String username) throws IOException
	{
		this.user = new User(username, this.socket);
		this.server.addUser(this.user);
		
		 DataOutputStream  outToClient = new DataOutputStream(socket.getOutputStream()); 
		 outToClient.writeBytes("OK\n");
	}
	
	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * Message -> void
	 *  This gets called when the client sends a message 
	 *  
	 *   Message: the message the user is trying to send
	 * @throws IOException 
	 */
	void route(Message message) throws IOException
	{
		User user = server.getUser(message.getReceiver());
		
		if(user == null)
		{
			// user is not found on either servers
			sendError();
			return;
		}
	
		if(user.getSocket() == null)
		{
		  	 String response = sendToPeerServer(message.getMessage());
			 DataOutputStream  outToSender = new DataOutputStream(this.socket.getOutputStream()); 
			 outToSender.writeBytes(response);
		}
		else
		{
			/*send message to the user*/
			 DataOutputStream  outToReceiver = new DataOutputStream(user.getSocket().getOutputStream()); 
			 outToReceiver.writeBytes(message.getMessage());
			 DataOutputStream  outToSender = new DataOutputStream(this.socket.getOutputStream()); 
			 outToSender.writeBytes("OK\n");
		}
	}

	/* void -> void
	 * makes a list of every member in the pool and sends it to the requester
	 */
	public void GetMemberListResponse() throws IOException {

		ArrayList<User> users = server.getUsers();
		String list = "LIST";
		for(User user : users)
		{
			list = list + "$" + user.getName();
		}
		list = list + "\n"; 
		DataOutputStream  outToClient = new DataOutputStream(socket.getOutputStream()); 

		outToClient.writeBytes(list);
	}
	
	/*
	 * void -> void
	 * Triggered whenever the Finite machine encounters an error 
	 */
	public void sendError() throws IOException
	{
		DataOutputStream  outToSender = new DataOutputStream(this.socket.getOutputStream()); 
		 outToSender.writeBytes("NOTOK\n");
	}
	
	/**
	 * Forwards a Given Message to the peer Server the closes the connection
	 * @param message
	 * @return 
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public String sendToPeerServer(String message) throws UnknownHostException, IOException
	{
		// Tries to JOIN the other server
		Socket serversocket = new Socket("127.0.0.1", server.getPeerServerPort());
		DataOutputStream outToServer = new DataOutputStream(serversocket.getOutputStream());
        outToServer.writeBytes("JOIN$server\n");
        
        BufferedReader inFromServer = 
                new BufferedReader(new
                InputStreamReader(serversocket.getInputStream())); 
        String response = inFromServer.readLine();
        if(response == null || !response.equals("OK"))
        {
            serversocket.close();
        	return "NOTOK\n";
        }
        
        outToServer.writeBytes(message);
        inFromServer = 
                new BufferedReader(new
                InputStreamReader(serversocket.getInputStream())); 
        response = inFromServer.readLine() + "\n";
        if(!response.equals("OK\n"))
        {
        	
        	response = "NOTOK\n";
        }
        serversocket.close();
        return response;
        
        
	
	}
}
