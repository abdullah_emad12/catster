package controller;

import eventlistener.ModelListener;

import java.util.ArrayList;

import model.Chat;
import model.Session;
import model.UsersRefresher;

import api.ChatAPI;
import api.ControllerAPI;
import view.ChatView;

public class ChatController {
	ChatAPI chatview;
	UsersRefresher refresher;
	ModelListener listener;
	Session session;
	public ChatController(ModelListener listener, Session session)
	{
		this.session = session;
		this.listener = listener;
		chatview = new ChatView((ControllerAPI)listener);
	}
	
	public void updateUsersList(ArrayList<Chat> chats)
	{
		chatview.revalidateChats(chats);
	}
	public void addMessage(String username)
	{
		chatview.refreshChatArea();
		chatview.throwNotification(username);
	}

	public void open()
	{
		refresher = new UsersRefresher(listener,session.getChats(), session.getConnectionSocket());
		refresher.start();
		this.chatview.setVisible(true);
	}


}
