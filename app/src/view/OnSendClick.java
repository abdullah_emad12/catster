package view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JScrollBar;



public class OnSendClick implements ActionListener{

	ChatView view;
	JScrollBar vertical;
	public OnSendClick(ChatView view, JScrollBar vertical)
	{
		this.vertical = vertical;
		this.view = view;
	}
	public void actionPerformed(ActionEvent e) {
		String msg = this.view.getMessageContent();
		
		// does not act on empty string
		if(msg.length() == 0)
		{
			return;
		}
		view.getMessageContent();
		view.addMessage(msg);
		vertical.setValue(vertical.getMaximum());
		view.clearSendText();
		view.getSender().sendMessage(msg, view.getCurrentChat().getUsername(), view.getCurrentChat());
		view.revalidate();
	}
	
	
}
