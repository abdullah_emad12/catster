package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import model.Chat;

public class OnUserSelect implements ActionListener{

	Chat chat; 
	ChatView view;
	public OnUserSelect(ChatView view, Chat chat, JButton button)
	{
		this.chat = chat;
		this.view = view;
	}
	public void actionPerformed(ActionEvent e) {
		
	
		Component[] buttons = this.view.getChatsButtons();
		for(int i = 0; i < buttons.length; i++)
		{
			if(!buttons[i].getBackground().equals(new Color(ChatView.notificationColor)));
			{
				buttons[i].setBackground(new Color(0xFFFFFFFF));
			}
		}
		view.getScrollbar().setValue(view.getScrollbar().getMaximum());
		JButton btn = (JButton) e.getSource();
		btn.setBackground(new Color(0xeafbff));
		view.setSendButtonEnabled(true);
		view.setCurrentChat(chat);
		view.revalidate();
	}

}
