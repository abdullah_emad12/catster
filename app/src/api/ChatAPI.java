package api;


import java.util.ArrayList;

import model.Chat;

public interface ChatAPI {
	/*  ArrayList<Chat> -> void
	 *  Takes a list of chats and updates the user panel
	 *  
	 *   ArrayList<Chat> chats: List of Chats that was refreshed from the server
	 */
	public void revalidateChats(ArrayList<Chat> chats);
	
	
	/* refreshChatArea
	 * void -> void
	 * Uses Model.Chat to refresh the chat Area and Display any new Messages
	 *
	 */
	public void refreshChatArea();
	
	/*
	 * String -> void
	 * throws a notification somehow to the user
	 * should only work when the user is not opening the chat
	 * at which the notification is destined 
	 * 
	 * string notification: the notification to be displayed
	 */
	public void throwNotification(String notification);

	
	
	/*
	 * Boolean -> void
	 * Sets the JFrame visibility to the given boolean
	 * 
	 *  Boolean visible: the new value of the JFrame visibility
	 *
	 */
	public void setVisible(boolean visible);
}
