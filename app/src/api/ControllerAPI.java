package api;

import model.Chat;

public interface ControllerAPI {

	public void sendMessage(String text, String receiver, Chat chat);
}
