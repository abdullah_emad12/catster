package exception;

public class InvalidUsernameException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username; // old usernamen
	public InvalidUsernameException(String username) {
		super(username + " is alread taken");
		this.username = username;
	}
	public InvalidUsernameException() {
		super("Invalid username");
		this.username = null;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
