package exception;

public class InvalidMessageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMessageException()
	{
		super("An error occured: message was not received. The user might have gone offline");
	}
}
