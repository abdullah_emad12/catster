package gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MessagePanel extends JPanel{

		private static final long serialVersionUID = 1L;
		private Shape shape;
		private static final int lineHeight = 10;
		private static final int lineWidth = 25;
	    public MessagePanel(String msg,Color color) {
	        super(new FlowLayout(FlowLayout.LEFT));
	        this.setBackground(color);
	        String msgFormatted;
	        
	        int lineCount = msg.length() / lineWidth;
	        if(msg.length() < lineWidth)
	        {
	        	msgFormatted= msg;
	        }
	        else
	        {
	        	msgFormatted = formatMessage(msg);
	        }
	        JLabel text = new JLabel(msgFormatted);
	        this.add(text);
	        this.setSize(lineWidth * 6,lineCount * lineHeight * 10);
	    }
	    
	    private static String formatMessage(String msg)
	    {
	    	// puts line breaks every 50 characters
	    	String[] words = msg.split(" ");
	        String msgHTML = "<html><body>"; 
        	int charCount = 0;

	        for(int i = 0; i < words.length; i++)
	        {
	        	// makes sure the word fits the line
	        	if(words[i].length() > lineWidth)
	        	{
	        		int divider = (int) (words[i].length() / lineWidth);
	        		int start = 0;
	        		for(int j = 1; j <= divider; j++)
	        		{
	        			msgHTML = msgHTML + words[i].substring(start, j * lineWidth) + "<br>";
	        			start = j * lineWidth;
	        		}
	        		msgHTML = msgHTML + words[i].substring(start);
	        		continue;
	        	}
	        	
	        	// breaks line by line
        		charCount += words[i].length() + 1;
        		if(charCount >= lineWidth)
        		{
        			charCount = 0;
        			msgHTML = msgHTML +  "<br>";
        			
        		}
        		msgHTML = msgHTML + " " + words[i];
        		
	        }
	        msgHTML = msgHTML + "</html></body>";
	        return msgHTML;
	    }
	    protected void paintComponent(Graphics g) {
	         g.setColor(getBackground());
	         g.fillRoundRect(0, 0, getWidth()-1, getHeight() - 1, 30, 30);
	        // super.paintComponent(g);
	    }
	    protected void paintBorder(Graphics g) {
	         g.setColor(getForeground());
	         g.drawRoundRect(0, 0, getWidth()-1, getHeight() - 1, 30, 30);
	    }
	    public boolean contains(int x, int y) {
	         if (shape == null || !shape.getBounds().equals(getBounds())) {
	             shape = new RoundRectangle2D.Float(0, 0, getWidth()-1, getHeight() - 1, 30, 30);
	         }
	         return shape.contains(x, y);
	    }
}
