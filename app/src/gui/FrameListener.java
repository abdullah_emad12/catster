package gui;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.JSplitPane;

public class FrameListener implements ComponentListener {

	JFrame frame;
	JSplitPane pane1;
	JSplitPane pane2;
	
	public FrameListener(JFrame frame, JSplitPane pane1, JSplitPane pane2)
	{
		this.frame = frame;
		this.pane1 = pane1;
		this.pane2 = pane2;
	}
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void componentResized(ComponentEvent arg0) {

		this.pane1.setDividerLocation((int)(frame.getWidth() * 0.25));
		this.pane2.setDividerLocation((int)(frame.getHeight() * 0.9));
	}

	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
