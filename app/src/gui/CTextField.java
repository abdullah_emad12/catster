package gui;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class CTextField extends JTextField{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CTextField(int x, int y, int width, int height)
	{
		super();
		
		super.setBackground(new Color(0xebfff5));
		Border roundedBorder = new LineBorder(new Color(0xebfff5), 5, true); // the third parameter - true, says it's round
		super.setBorder(roundedBorder);
		super.setSize(width,height);
		super.setLocation(x,y);
	}

}
