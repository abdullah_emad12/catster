package gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import static javax.swing.ScrollPaneConstants.*;

@SuppressWarnings("unused")
public class ChatPane extends JScrollPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image img;
	
	public ChatPane(JComponent component, int width, int height )
	{
		super(component);
		this.setSize(width, height);
		this.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
	}

	
}
