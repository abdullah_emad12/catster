package eventlistener;

import java.util.EventListener;

public interface MessageListener extends EventListener{
	public void onReceivedMessage(String message);
}
