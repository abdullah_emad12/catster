package eventlistener;

import java.util.EventListener;

import model.Chat;


public interface ModelListener extends EventListener{
	public void onConnected(State state);
	public void onReceivedMessage(Chat chat);
	public void onUpdatedChatList();
	public void changeSession();
}
