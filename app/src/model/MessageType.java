package model;

public enum MessageType {
	RECEIVED, SENT
}
