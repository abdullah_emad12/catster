package model;

public enum ChatState {
	CONNECTED, JOINED, CLOSED;
}
