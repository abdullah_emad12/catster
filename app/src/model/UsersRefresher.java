package model;

import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import eventlistener.ModelListener;


/**
 * 
 * @author Abdullah
 * This Class is responsible for getting all the online users that are connected to
 * the other server
 *
 */
public class UsersRefresher extends Thread {

	ArrayList<Chat> users;
	Socket socket;
	ModelListener listener;
	public static final int sleepTime = 3000; 
	public UsersRefresher(ModelListener listener, ArrayList<Chat> users, Socket socket) 	
	{
		this.users = users;
		this.socket = socket;
		this.listener = listener;
	}
	public void run()
	{
		while(true)
		{
			
			try
			{
				sleep(sleepTime);
				

				DataOutputStream  outToClient = new DataOutputStream(socket.getOutputStream());
				outToClient.writeBytes("GET\n");
          	  
				
			}
			catch(Exception e)
			{
			}
		}
	}
	

}
