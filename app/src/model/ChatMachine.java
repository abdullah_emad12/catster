package model;
import java.io.IOException;
import java.util.ArrayList;



import exception.*;
public class ChatMachine {

	private ChatState currentState;
	private Session session;
	
	
	public ChatMachine(Session session)
	{
		this.session = session;
		currentState = ChatState.CONNECTED;
	}
	
	public ChatState getCurrentState()
	{
		return this.currentState;
	}
	
	/**
	 * String -> void 
	 * Takes input from the server and reacts accordingly
	 * 
	 * String: the string recevied from the server
	 * @throws InvalidUsernameException 
	 * @throws InvalidMessageException 
	 * @throws IOException 
	 */
	public void onInput(String input) throws InvalidUsernameException, InvalidMessageException, IOException
	{
		switch(currentState)
		{
			case CONNECTED:
				parseConnected(input);
				break;
			case JOINED:
				parseJoined(input);
				break;
			default:
				break;
		}
	}
	
	/**
	 * String -> void
	 * Parse the input and calls the necessary function assuming the machine 
	 * is in connected state
	 * @throws InvalidUsernameException 
	 */
	private void parseConnected(String input) throws InvalidUsernameException
	{
		/*Error, probably the username is not available*/
		if(input.equals("NOTOK"))
		{
			session.onJoined(eventlistener.State.FAILURE);
			if(session.getName() != null)
			{
				throw new InvalidUsernameException(session.getName());
			}
			else
			{
				throw new InvalidUsernameException();
			}
		}
		
		/*The username was accepted*/
		if(input.equals("OK"))
		{
			session.onJoined(eventlistener.State.SUCCESS);
			currentState = ChatState.JOINED;
		}
	}
	
	/**
	 * String -> void
	 * Parse the input and calls the necessary function assuming the machine 
	 * is in connected state
	 * @throws InvalidMessageException 
	 * @throws IOException 
	 */
	private void parseJoined(String input) throws InvalidMessageException, IOException
	{
		/*close connection*/
		if(input == null || input.equals("BYE"))
		{
			currentState = ChatState.CLOSED;
			return;
		}	
		/*No need to process the incoming message*/
		if(input.length() < 4)
		{
			return;
		}
		/*An error occured*/
		if(input.equals("NOTOK"))
		{
			throw new InvalidMessageException(); 
		}
		
		/*A list of the username was received*/
		if(input.substring(0,4).equals("LIST"))
		{
			String[] members = input.split("\\$");
			ArrayList<String> membersList = new ArrayList<String>();
			for(int i = 1; i < members.length; i++)
			{
				membersList.add(members[i]);
				session.UpdateMembersList(membersList);
			}
		}
		/*A new message was received*/
		else if(input.substring(0, 4).equals("SEND"))
		{
			/*sanity checks*/
			String[] args = input.split("\\$");

			/*badly formatted message*/
			if(args.length != 5)
			{
				System.out.println("A badly formated message was received and dropped");
				session.responedToServer("NOTOK");
				return;
			}
			boolean numeric = args[4].matches("-?\\d+(\\.\\d+)?");
			
			// checks that the ttl is numeric
			if(!numeric)
			{
				System.out.println("A badly formated message was received and dropped");
				session.responedToServer("NOTOK");
				return;
			}
			int ttl = Integer.parseInt(args[4]);
			Message message = new Message(args[1], args[2], args[3], ttl, MessageType.RECEIVED);
			session.onReceivedMessage(message);
		}
	}
}
