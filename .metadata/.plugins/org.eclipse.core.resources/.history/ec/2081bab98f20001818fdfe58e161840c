package view;

import gui.ChatPane;
import gui.FrameListener;
import gui.MessagePanelArea;
import gui.Notification;
import gui.RoundJTextField;
import gui.SendButton;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Scrollbar;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import api.ChatAPI;
import api.ControllerAPI;


import model.Chat;
import model.Message;
import model.MessageType;
import static javax.swing.ScrollPaneConstants.*;


public class ChatView extends JFrame implements ChatAPI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JSplitPane main;
	private JSplitPane chat;
	private JPanel chatArea;
	private JPanel users;
	private JPanel tools;
	private RoundJTextField msgBox;
	private SendButton sendbtn;
	private Chat currentChat;
	private ControllerAPI sender;
	private JScrollBar scrollbar;
	
	static final int notificationColor = 0xfcb08a;
	
	public ChatView(ControllerAPI sender)
	{
		super("Catster");
		this.currentChat = null;
		this.setSender(sender);
		/*Initializes the JFrame*/
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(screenSize);
		this.setLocation(0, 0);
		ImageIcon icon = new ImageIcon("Art/icon.png");
		this.setIconImage(icon.getImage());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		
		// chat area (where all the messages will be displayed)
        this.chatArea = new JPanel(new GridLayout(0,1));
        this.chatArea.setBackground(new Color(0xffefe8));
        this.chatArea.setVisible(true);
        ChatPane contentpane = new ChatPane(chatArea,"Art/chat-bg.png", (int)(screenSize.getWidth() * 0.9), (int)(screenSize.getWidth() * 0.6));
        scrollbar = contentpane.getVerticalScrollBar();
        
        // holds the chat textbox and the buttons
        tools = new JPanel(null);
        tools.setOpaque(true);
        tools.setBackground(new Color(0xfce3d1));
        tools.setVisible(true);
        Dimension dim = new Dimension();
        dim.setSize(screenSize.getWidth() * 0.75, screenSize.getHeight() * 0.1);
        tools.setSize(dim);
        
        // message box 
        msgBox = new RoundJTextField(250);
        msgBox.setBounds((int)(tools.getWidth() * 0.05),(int)(tools.getHeight() * 0.15), 700, 35);
		

        
        // Send Button
        int relativeLocationX = (int) msgBox.getLocation().getX() + msgBox.getWidth() + 15;
        int relativeLocationY = (int) msgBox.getLocation().getY() - 5 ;
        sendbtn = new SendButton("Art/button.png", relativeLocationX, relativeLocationY, 40, 40);
        sendbtn.addActionListener(new OnSendClick(this, contentpane.getVerticalScrollBar()));
        sendbtn.setEnabled(false);

        tools.add(msgBox);	   

        tools.add(sendbtn);
        
        // where all the online users will be displayed
        users = new JPanel(new GridLayout(11,1));
        users.setBackground(new Color(0xffffffff));
        users.setVisible(true);
        JLabel header =new JLabel("<html><body><h1>Online Users</h1></body></html>");
        users.add(header);
        JScrollPane scroll = new JScrollPane(users);
		scroll.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
		
 
        
        //(Chat Pane) a two sections split pane that has the chat Area and the tools
        chat = new JSplitPane();
        chat.setDividerSize(0);
        chat.setDividerLocation(chatArea.getHeight());
        chat.setOrientation(JSplitPane.VERTICAL_SPLIT);
        chat.setTopComponent(contentpane);
        chat.setBottomComponent(tools);
        chat.setEnabled(false);        
         
       //(main pane) A three section chat Pane that has the chat Area and the tools
        main = new JSplitPane();
        main.setSize(screenSize);
        main.setDividerSize(0);
        main.setDividerLocation((int)(screenSize.getWidth() * 0.1));
        main.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        main.setLeftComponent(users);
        main.setRightComponent(chat);
        main.setEnabled(false);
      
        
        this.add(main);
        // resizes the splitpane when the windows get resize
        this.addComponentListener(new FrameListener(this, main, chat));
	}
	
	/*
	 * Adds a new button the the ArrayList
	 * clears the users list and add a new List
	 */
	public void revalidateChats(ArrayList<Chat> chats)
	{
        Component[] comp = this.users.getComponents();
		for(Chat chat : chats)
		{
			boolean contains = false;
			for(int i = 0; i < comp.length; i++)
			{
				// makes sure not to add already existing user
				if(comp[i] instanceof JButton)
				{
					if(((JButton)comp[i]).getText().equals(chat.getUsername()))
					{
						contains = true;
					}
				}
			}
			if(!contains)
			{
				JButton chtbtn = new JButton(chat.getUsername());
				// selects the first chat if one was not selected already
				if(currentChat == null)
				{
					chtbtn.setBackground(new Color(0xeafbff));
					currentChat = chat;
					this.setSendButtonEnabled(true);
				}
				else
				{
					chtbtn.setBackground(new Color(0xFFFFFFFF));
				}
				chtbtn.addActionListener(new OnUserSelect(this, chat, chtbtn));
				this.users.add(chtbtn);
			}
		}
		
		this.revalidate();
		this.repaint();
	}

	public void setCurrentChat(Chat chat)
	{
		
		this.currentChat = chat;
		this.refreshChatArea();
	}
	
	/**
	 * Message, MessageType -> void
	 * Adds new Message to chat Area and the current Chat
	 * 
	 * Message msg: message to be to the chat Area
	 * MessageType type: received
	 * 
	 * Note: should Only be used to add the sent messages to the chatArea
	 */
	public void addMessage(String msg) {
		this.chatArea.add(new MessagePanelArea(msg, MessageType.SENT));
		this.revalidate();
	}


	/**
	 * String -> void
	 * Generates a notification after receiving a message from a certain user
	 * 
	 * String notification: typically the name of the user
	 */
	public void throwNotification(String notification) {
		if(currentChat == null || notification.equals(currentChat.getUsername()))
		{
			return;
		}
		try {
			Notification.tone(900, 600);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		Component[] buttons = this.users.getComponents();
		for(int i = 0; i < buttons.length; i++)
		{
			if(buttons[i] instanceof JButton &&
					((JButton)buttons[i]).getText().equals(notification))
			{
				buttons[i].setBackground(new Color(notificationColor));
			}
		}
		this.revalidate();
		this.repaint();
	}
	public Component[] getChatsButtons()
	{
		return this.users.getComponents();
	}
	public String getMessageContent()
	{
		return this.msgBox.getText();
	}

	public ControllerAPI getSender() {
		return sender;
	}

	public void setSender(ControllerAPI sender) {
		this.sender = sender;
	}

	public Chat getCurrentChat() {
		return currentChat;
	}
	public void setSendButtonEnabled(boolean enabled)
	{
		this.sendbtn.setEnabled(enabled);
	}
	public void clearSendText()
	{
		this.msgBox.setText("");
		this.revalidate();
	}

	public void refreshChatArea() {
		if(currentChat != null)
		{
			chatArea.removeAll();
			for(Message message : currentChat.gettMessages())
			{
				this.chatArea.add(new MessagePanelArea(message.getContent(), message.getType()));
				
			}
			chatArea.revalidate();
			chatArea.repaint();
			scrollbar.setValue(scrollbar.getMaximum());
		}
	}
	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
	}
	public Scrollbar getScrollbar()
	{
		return this.scrollbar;
	}
}
