package model;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.io.InputStreamReader;


import eventlistener.ModelListener;
import exception.InvalidMessageException;
import exception.InvalidUsernameException;

public class Session extends Thread{
	private String username; // name of the client
	private Socket connectionSocket; // socket connected to the server
	private ArrayList<Chat> chats; // list of active chats
	private ChatMachine fsm;
	private DataOutputStream outToServer;
	private ModelListener listener;
	public Session(ModelListener listener)
	{
		this.listener = listener;
		this.chats = new ArrayList<Chat>();
		this.username = null;
		this.connectionSocket = null;
		this.outToServer = null;
		this.fsm = new ChatMachine(this);
	}
	
	public Socket getConnectionSocket()
	{
		return this.connectionSocket;
	}
	/**
	 * Socket -> Void
	 * Does necessary initializations
	 */
	public void init(Socket connectionSocket)
	{
		this.connectionSocket = connectionSocket;
		if(this.getState() == Thread.State.NEW)
		{
			this.start();
		}
	}
	/**
	 * Setters and Getters
	 * @return
	 */

	public ArrayList<Chat> getChats() {
		return chats;
	}
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String name) {
		this.username = name;
	}
	public ChatMachine getFsm() {
		return fsm;
	}
	public void setFsm(ChatMachine fsm) {
		this.fsm = fsm;
	}
	
	
	/**
	 * String -> void
	 * Sends a join request to the server
	 * @throws IOException 
	 */
	public void Join(String username) throws IOException
	{
		this.username = username;
		System.out.println(connectionSocket.getOutputStream());
		this.outToServer = new DataOutputStream(connectionSocket.getOutputStream());
        outToServer.writeBytes("JOIN$" + username + "\n");
	}
	
	/**
	 * void -> void
	 * Sends a GET request to get the list of users connected to sever
	 * Note: this has nothing to do with the HTTP Get Request
	 * @throws IOException 
	 */
	public void GetMembersList() throws IOException
	{
		outToServer = new DataOutputStream(connectionSocket.getOutputStream());
        outToServer.writeBytes("GET" + "\n");
	}
	/**
	 * ArrayList -> Void
	 * Updates the arrayList of the users
	 * 
	 */
	public void UpdateMembersList(ArrayList<String> members)
	{
		this.chats.removeAll(this.chats);
		for(String user : members)
		{
			System.out.println(username);
			if(user.equals(this.username))
			{
				this.chats.add(new Chat(user));
			}
		}
		listener.onUpdatedChatList();
	}

	/**
	 * String -> Void
	 * Responds to the server with a Given String
	 * @throws IOException 
	 * 
	 */
	public void responedToServer(String response) throws IOException
	{
		outToServer = new DataOutputStream(connectionSocket.getOutputStream());
        outToServer.writeBytes(response + "\n");
	}
	
	
	/**
	 * Message -> void
	 * sends a message to the server to route it to the reciever
	 */
	public void sendMessage(Message message)
	{

		try {
			outToServer = new DataOutputStream(connectionSocket.getOutputStream());
	        outToServer.writeBytes(message.getMessage() + "\n");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Message -> void
	 * Adds a new received message to the destination chat
	 */
	public void onReceivedMessage(Message message)
	{
		/*looks for an existing chat*/
		for(Chat chat : this.chats)
		{
			if(chat.getUsername() == message.getSender())
			{
				chat.setMessage(message);
				return;
			}
		}
		/*creates a new chat*/
		Chat chat = new Chat(message.getSender());
		chat.setMessage(message);
	}
	public void onJoined(eventlistener.State state)
	{
		if(state == eventlistener.State.FAILURE)
		{
			this.username = null;
		}
		listener.onConnected(state);
	}
	
	
	/**
	 * void -> String 
	 * Keeps Listening on the connection socket for input from the server
	 * returns received Strings from the server
	 * @throws IOException 
	 */
	private String Listen() throws IOException
	{
	  	  
  	        BufferedReader inFromServer = 
  	                new BufferedReader(new
  	                InputStreamReader(this.connectionSocket.getInputStream()));
  	        String msg = inFromServer.readLine();
  	        
  	        if(msg == null)
  	        {
  	        	msg = "BYE";
  	        }
  	        return msg;
	}
	
	/**
	 * void -> void
	 * Thread interface
	 * keeps Listening for server messages
	 */
	public void run()
	{
		while(true)
		{
			try {
				String msg = this.Listen();
				this.fsm.onInput(msg);
				System.out.println(msg);
				System.out.print(this.fsm.getCurrentState());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InvalidUsernameException e) {
				e.printStackTrace();
			} catch (InvalidMessageException e) {
				e.printStackTrace();
			}
		}
	}
	
}


